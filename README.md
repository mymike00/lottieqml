# LottieQML

lottie-qml lib to render lottie animations into a canvas

The Lottie JS version shipped with the `lottie-qml` submodule isn't the latest. To update, run

```
./get_lottie_upstream.sh
```

and you will get the version `5.7.6`. Check at https://github.com/airbnb/lottie-web if there is a new version available.

## License

Copyright (C) 2021  Michele Castellazzi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
